'use strict';

var mongoose = require('mongoose'),
    Thing = mongoose.model('Thing'),
    http = require('http');

/**
 * Utility functions
 */

/**
 * Get the start date as expected by yahoo - day=e, month=d, year=f
 * @param dateStr - in the format yyyy-mm-dd
 * @returns {string}
 */
var parseYahooEndDate = function(str) {
  var tokens = str.split('-');
  return '&d=' + (+tokens[1] - 1) +
      '&e=' + (+tokens[2]) +
      '&f=' + (+tokens[0]);
};

/**
 * Get the end date as expected by yahoo - day=b, month=a, year=c
 * @param dateStr - in the format yyyy-mm-dd
 * @returns {string}
 */
var parseYahooStartDate = function(str) {
  var tokens = str.split('-');
  return '&a=' + (+tokens[1] - 1) +
      '&b=' + (+tokens[2]) +
      '&c=' + (+tokens[0]);
};

var parseCSVToJSON = function(csv) {
  var obj = {},
      lines = csv.split('\n'),
      keys = lines.shift().split(',');

  // return error if response isn't as expected
  if (keys.length !== 7) {
    return {
      error: "No data found for symbol provided."
    };
  }
  // last line is blank - remove it
  lines.pop();

  lines.forEach(function(item, i) {
    var fields = item.split(','),
        key = '' + (new Date(fields[0])).getTime();
    obj[key] = {};
    keys.forEach(function(k, j) {
      obj[key][k] = fields[j];
    });
  });
  return obj;
};

/**
 * API methods
 */

/**
 * Get price data from yahoo finance
 * @param req
 * @param res
 */
exports.priceDataYahoo = function(req, res) {
  var options = {
    host: 'http://real-chart.finance.yahoo.com',
    path: '/table.csv?'
  };

  // return error if no symbol has been provided
  if (!req.query.symbol) {
    res.statusCode=400;
    res.json({error: 'No symbol provided'});
    return;
  }

  // construct the path and query params
  options.path += 's=' + req.query.symbol;
  if (req.query.enddate) {
    options.path += parseYahooEndDate(req.query.enddate);
  }
  if (req.query.interval) {
    options.path += '&g=' + req.query.interval;
  } else {
    options.path += '&g=d';
  }
  if (req.query.startdate) {
    options.path += parseYahooStartDate(req.query.startdate);
  }
  options.path += '&ignore=.csv';

  // make the request - Note that instead of passing just the
  // options to http.get, the url is made by concatenating the
  // host and path, as the host on its own does not resolve, and
  // so an enotfound error was being thrown
  http.get(options.host + options.path, function(r) {
    var data = '';
    r.setEncoding('utf8');
    r.on('data', function(d) {
      data += d;
    });
    r.on('end', function() {
//      console.log('data (server): ' + data);
      res.json(parseCSVToJSON(data));
    });
    r.on('error', function(e) {
      res.statusCode = 500;
      res.send({error: 'Error from Yahoo price data service'});
    });
  });
};

/**
 * Get awesome things
 */
exports.awesomeThings = function(req, res) {
  return Thing.find(function (err, things) {
    if (!err) {
      return res.json(things);
    } else {
      return res.send(err);
    }
  });
};