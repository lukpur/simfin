'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('simFinApp'));
  beforeEach(module('simFin.priceDataServices'));

  var MainCtrl,
    scope,
    $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function (_$httpBackend_, $controller, $rootScope) {
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('/api/pricedata/yahoo?symbol=^ftse&interval=d')
        .respond({});
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should get price data on initialise', function () {
    expect(scope.ohlcData.priceData).toBeUndefined();
    $httpBackend.flush();
    expect(scope.ohlcData.priceData).toEqual({});
  });
});
