describe('Service: ohlcDataService', function() {

  // load the service module
  beforeEach(module('simFin.priceDataServices'));
  beforeEach(module('simFin.configurationService'));
  beforeEach(module('simFin.mock.priceDataServices'));

  var _ohlcDataService,
      _priceDataService,
      $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function (_$httpBackend_, ohlcDataService, yahooDataService) {
    _ohlcDataService = ohlcDataService;
    // get a reference to the yahooDataService (mock) so that
    // we can spy on it
    _priceDataService = yahooDataService;
    $httpBackend = _$httpBackend_;
  }));

  it('should have a getPriceData method', function() {
    expect(typeof _ohlcDataService.getPriceData).toBe('function');
  });

  it('should call the underlying data service with just the symbol', function() {
    spyOn(_priceDataService, 'getPriceData').andCallThrough();
    _ohlcDataService.loadPriceData('GOOG');
    expect(_priceDataService.getPriceData).toHaveBeenCalledWith('GOOG');
  });

  it('should call the underlying data service with the symbol and startDate', function() {
    spyOn(_priceDataService, 'getPriceData').andCallThrough();
    _ohlcDataService.loadPriceData('GOOG', {startDate:'2000-01-01'});
    expect(_priceDataService.getPriceData).toHaveBeenCalledWith('GOOG', {startDate: '2000-01-01'});
  });

  it('should call the underlying data service with the symbol and endDate', function() {
    spyOn(_priceDataService, 'getPriceData').andCallThrough();
    _ohlcDataService.loadPriceData('GOOG', {endDate: '2014-06-14'});
    expect(_priceDataService.getPriceData).toHaveBeenCalledWith('GOOG', {endDate: '2014-06-14'});
  });

  it('should call the underlying data service with the symbol, startDate and endDate', function() {
    spyOn(_priceDataService, 'getPriceData').andCallThrough();
    _ohlcDataService.loadPriceData('GOOG', {startDate:'2000-01-01', endDate: '2014-06-14'});
    expect(_priceDataService.getPriceData).toHaveBeenCalledWith('GOOG', {startDate: '2000-01-01', endDate: '2014-06-14'});
  });

  it('should have a getCurrentSymbol method', function() {
    expect(typeof _ohlcDataService.getCurrentSymbol).toBe('function');
  });

  it('should return the last requested symbol', function() {
    expect(_ohlcDataService.getCurrentSymbol()).toBeUndefined();
    _ohlcDataService.loadPriceData('GOOG');
    expect(_ohlcDataService.getCurrentSymbol()).toBe('GOOG');
  })
});