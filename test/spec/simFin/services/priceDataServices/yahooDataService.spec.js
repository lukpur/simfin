describe('Service: yahooDataService', function() {

  // load the service module
  beforeEach(module('simFin.priceDataServices'));
  beforeEach(module('simFin.configurationService'));

  var _yahooDataService,
      $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function (_$httpBackend_, yahooDataService) {
    _yahooDataService = yahooDataService;
    $httpBackend = _$httpBackend_;
  }));

  it('should have a getPriceData method', function() {
    expect(typeof _yahooDataService.getPriceData).toBe('function');
  });

  it('should request just the symbol if no dates are provided', function() {
    $httpBackend.expectGET('/api/pricedata/yahoo?symbol=GOOG')
        .respond('success');
    _yahooDataService.getPriceData('GOOG');
    $httpBackend.flush();
  });

  it('should request the symbol and startDate', function() {
    $httpBackend.expectGET('/api/pricedata/yahoo?symbol=GOOG&startdate=2000-01-01')
        .respond('success');
    _yahooDataService.getPriceData('GOOG', {startDate:'2000-01-01'});
    $httpBackend.flush();
  });

  it('should request the symbol and endDate', function() {
    $httpBackend.expectGET('/api/pricedata/yahoo?symbol=GOOG&enddate=2010-12-31')
        .respond('success');
    _yahooDataService.getPriceData('GOOG', {endDate:'2010-12-31'});
    $httpBackend.flush();
  });

  it('should request the symbol, startDate and endDate', function() {
    $httpBackend.expectGET('/api/pricedata/yahoo?symbol=GOOG&startdate=2000-01-01&enddate=2010-12-31')
        .respond('success');
    _yahooDataService.getPriceData('GOOG', {startDate: '2000-01-01', endDate:'2010-12-31'});
    $httpBackend.flush();
  });

});