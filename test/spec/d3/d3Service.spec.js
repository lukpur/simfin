describe('d3Service', function() {

  var _d3Service;

  beforeEach(module('d3'));

  beforeEach(function() {
    inject(function(d3Service) {
      _d3Service = d3Service;
    })
  })

  it('should return the d3 instance if it is loaded', function() {
    // add mock d3 to window
    window.d3  = {message: 'Got D3'};
    expect(_d3Service.getD3().message).toBe('Got D3');
  });


  it('should return an object with an error message if D3 is not found', function() {
    var saveD3 = window.d3;
    window.d3 = undefined;
    expect(_d3Service.getD3().error).toBeDefined();
    window.d3 = saveD3;
  });
});