angular.module('d3', [] )
    .factory('d3Service', function() {
      return {
        getD3 : function() {
          if(!window.d3) {
            return {
              error: "D3 not found. Ensure that the d3.js library is loaded before angular."
            }
          }
          return window.d3;
        }
      }
    })
