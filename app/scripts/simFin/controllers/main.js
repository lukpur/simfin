'use strict';

angular.module('simFinApp')
  .controller('MainCtrl', function ($scope, $http, ohlcDataService, d3Service, timeFrameService) {
    $scope.ohlcData = {};
    $scope.unsavedData = {};
    var defaultSymbol = '^ftse',
        defaultInterval = 'd',
        defaultFrameSize = 120,
        loadData = function(symbol) {
          ohlcDataService.loadPriceData(symbol, {interval: $scope.ohlcData.currentInterval}).then(function() {
            $scope.ohlcData.priceData = timeFrameService.getDataFrame();
          });
        };

    // Set up default data
    timeFrameService.setFrameSize(defaultFrameSize);
    $scope.ohlcData.currentInterval = defaultInterval;
    loadData(defaultSymbol);

    $scope.ohlcData.currentSymbol = ohlcDataService.getCurrentSymbol();
    $scope.ohlcData.currentFrameSize = defaultFrameSize;
    $scope.unsavedData.symbol = $scope.ohlcData.currentSymbol;
    $scope.d3 = d3Service.getD3();

    $scope.setSymbol = function(newSymbol) {
      $scope.ohlcData.currentSymbol = newSymbol;
      loadData($scope.ohlcData.currentSymbol);
    };

    $scope.updateFrameSize = function() {
      timeFrameService.setFrameSize($scope.ohlcData.currentFrameSize);
      $scope.reloadData();
    };

    $scope.reloadData = function() {
      loadData($scope.ohlcData.currentSymbol);
    }

    $scope.step = function(amount) {
      timeFrameService.setEndDate(new Date(timeFrameService.getEndDate().getTime() + (amount * 1000 * 60 * 60 * 24)));
      loadData($scope.ohlcData.currentSymbol);
    };
  });
