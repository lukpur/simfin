angular.module('simFinApp')
.directive('priceChart', ['$window', 'd3Service', 'config', 'xScaleService', function($window, d3Service, config, xScaleService) {
      var opts = config;
      return {
        restrict: 'E',
        scope: {
          data: '='
        },
        link: function(scope, elem, attrs) {
          var d3, svg, oldData,
              plotWidth, plotHeight,
              svgWidth, svgHeight,
              plot, xAxisG, yAxisG,
              xScale, yScale,
              xScalePrevious, yScalePrevious,
              xAxis, yAxis;

          d3 = d3Service.getD3();
          elem.css({'min-width' : opts.minWidth,
                    'overflow-x' : 'scroll'});

          svg = d3.select(elem[0]).append('svg')
              .style({'width' : '100%'});

          // Fire the angular digest loop when the window is resized
          window.onresize = function() {
            scope.$apply();
          };

          // Utility method to return an array of keys for the data
          scope.getKeys = function(d) {
            return Object.keys(d).reverse();
          }
          // re-render when window resized
          scope.$watch(function() {
            return angular.element($window)[0].innerWidth;
          }, function() {
            scope.render(scope.data);
          });

          // re-render when data changes
          scope.$watch('data', function(){
            scope.render(scope.data);
          });

          scope.render = function(data) {
            if(data) {
              scope.initializeDimensions();
              scope.calculateScales(data);
              if (!oldData) {
                oldData = data;
                scope.initializeChart(data);
              }
              scope.updateChart(data);
            }
          };

          scope.initializeChart = function(data) {
            var keys = scope.getKeys(data),
                enterRoot;
            svg.attr('height', svgHeight);
            svg.selectAll('*').remove();
            plot = svg.append('g')
                .attr('transform', 'translate(' + opts.margin.left + ',' + opts.margin.right + ')');

            // Initialize axes
            xAxisG = svg.append('g')
                .attr('class', 'axis x')
                .attr('transform', 'translate(' + opts.margin.left  +',' + (plotHeight + opts.margin.top) + ')');
            yAxisG = svg.append('g')
                .attr('class', 'axis y')
                .attr('transform', 'translate(' + opts.margin.left + ',' + opts.margin.top +')');
            xAxisG.call(xAxis);
            yAxisG.call(yAxis);

            // Initialize price points
            enterRoot = plot.selectAll('.pricePoint')
                .data(keys, function(d) {
                  return d;
                })
                .enter()
                  .append('g')
                  .attr('class', function(d) {
                    return 'pricePoint ' + (data[d].Close > data[d].Open ? 'up' : 'down');
                  })
                  .attr('transform', function(d) {
                    return 'translate(' + xScale(d) + ',0)';
                  });
            // Bar body
            enterRoot
              .append('line')
                .attr('class', 'bar-body')
                .attr('x1', 0)
                  .attr('y1', function(d){
                    return yScale(data[d].Low);
                  })
                  .attr('x2', 0)
                  .attr('y2', function(d) {
                    return yScale(data[d].High);
                  });
            // Open notch
            enterRoot
              .append('line')
                .attr('class', 'open-notch')
                .attr('x1', -opts.notchWidth)
                .attr('y1', function(d) {
                  return yScale(data[d].Open);
                })
                .attr('x2', 0)
                .attr('y2', function(d) {
                  return yScale(data[d].Open);
                });
            // Close notch
            enterRoot
              .append('line')
                .attr('class', 'close-notch')
                .attr('x1', opts.notchWidth)
                .attr('y1', function(d) {
                  return yScale(data[d].Close);
                })
                .attr('x2', 0)
                .attr('y2', function(d) {
                  return yScale(data[d].Close);
                });
          };

          scope.updateChart = function(data) {
            var keys = scope.getKeys(data),
                dataRoot, updateRoot, enterRoot,
                _data = data;
            svg.transition(opts.transitionDuration).attr('height', svgHeight);
            xAxisG.transition(opts.transitionDuration).attr('transform', 'translate(' + opts.margin.left  +',' + (plotHeight + opts.margin.top) + ')')
                .call(xAxis);
            yAxisG.transition(opts.transitionDuration).call(yAxis);

            // Move the x coordinate to new position
            dataRoot = plot.selectAll('.pricePoint').data(keys, function(d) {
              return d;
            });
            dataRoot.exit().transition(opts.transitionDuration)
                .attr('transform', function(d) {
                  return 'translate(' + xScale(this.__data__) + ',0)';
                })
                .attr('opacity', 0)
                .remove();

            // *** ENTERING data points ***
            enterRoot = dataRoot.enter()
              .append('g')
                .attr('class', function(d) {
                  return 'pricePoint ' + (data[d].Close > data[d].Open ? 'up' : 'down');
                })
                // Using the previous scale as the initial x coordinate so that
                // the bar transitions in nicely from the correct direction
                .attr('transform', function(d) {
                  return 'translate(' + xScalePrevious(d) + ',0)';
                })
                .attr('opacity', 0);
            // Bar body
            enterRoot
              .append('line')
                .attr('class', 'bar-body')
                .attr('x1', 0)
                .attr('y1', function(d){
                  return yScalePrevious(_data[d].Low);
                })
                .attr('x2', 0)
                .attr('y2', function(d) {
                  return yScalePrevious(_data[d].High);
                });
            // Open notch
            enterRoot
              .append('line')
                .attr('class', 'open-notch')
                .attr('x1', -opts.notchWidth)
                .attr('y1', function(d) {
                  return yScalePrevious(_data[d].Open);
                })
                .attr('x2', 0)
                .attr('y2', function(d) {
                  return yScalePrevious(_data[d].Open);
                });
            // Close notch
            enterRoot
              .append('line')
                .attr('class', 'close-notch')
                .attr('x1', opts.notchWidth)
                .attr('y1', function(d) {
                  return yScalePrevious(_data[d].Close);
                })
                .attr('x2', 0)
                .attr('y2', function(d) {
                  return yScalePrevious(_data[d].Close);
                });

            // *** UPDATED data points (includes the entering data points) ***
            updateRoot = dataRoot.transition(opts.transitionDuration)
                .attr('transform', function(d) {
                  return 'translate(' + xScale(d) + ',0)';
                })
                .attr('opacity', 1);
            // Move the price bar body to new location
            updateRoot.selectAll('.bar-body')
                .attr('x1', 0)
                .attr('y1', function(d){
                  return yScale(_data[d].Low);
                })
                .attr('x2', 0)
                .attr('y2', function(d) {
                  return yScale(_data[d].High);
                });
            // Move the price bar opening notch to new location
            updateRoot.selectAll('.open-notch')
                .attr('x1', -opts.notchWidth)
                .attr('y1', function(d) {
                  return yScale(_data[d].Open);
                })
                .attr('x2', 0)
                .attr('y2', function(d) {
                  return yScale(_data[d].Open);
                });
            // Move the price bar closing notch to new location
            updateRoot.selectAll('.close-notch')
                .attr('x1', opts.notchWidth)
                .attr('y1', function(d) {
                  return yScale(_data[d].Close);
                })
                .attr('x2', 0)
                .attr('y2', function(d) {
                  return yScale(_data[d].Close);
                });
          };

          scope.calculateScales = function(data) {
            // scalePadding is so that the series is not
            // drawn right against the axis for the extreme
            // values of the data set
            var yScalePadding,
                keys = scope.getKeys(data),
                minLow, maxHigh;
            minLow = d3.min(keys, function(d) {
              return +data[d]['Low'];
            });
            maxHigh = d3.max(keys, function(d) {
              return +data[d]['High'];
            });
            yScalePadding = (maxHigh - minLow) * opts.yScalePaddingPercent;

            // keep a reference to the old scale for entering node transitions
            xScalePrevious = xScale;
            yScalePrevious = yScale;

            // We use an xScaleService to provide a shared xScale across the charts
            xScale = xScaleService.createNewScale(keys, plotWidth);
            yScale = d3.scale.linear().range([plotHeight, 0])
                .domain([minLow - yScalePadding, maxHigh + yScalePadding]);
            xAxis = d3.svg.axis().scale(xScale).ticks(8);
            yAxis = d3.svg.axis().scale(yScale).orient('left');
          };

          scope.initializeDimensions = function() {
            svgWidth = d3.select(elem[0]).node().offsetWidth;
            svgHeight = ~~(svgWidth * (1/1.618));
            plotWidth = svgWidth - opts.margin.right - opts.margin.left;
            plotHeight = svgHeight - opts.margin.top - opts.margin.bottom;
            console.log('plot width: ' + plotWidth);
          }
        }
      }
    }]);