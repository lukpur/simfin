angular.module('simFin.priceDataServices')
    .factory('yahooDataService', ['$http', 'priceDataEndpoint', function($http, priceDataEndpoint) {
      return {
        getPriceData: function(symbol, opts) {
          opts = opts || {};
          return $http.get(priceDataEndpoint + 'yahoo?symbol=' + symbol
            + (opts.startDate ? '&startdate=' + opts.startDate : '')
            + (opts.interval ? '&interval=' + opts.interval : '')
            + (opts.endDate ? '&enddate=' + opts.endDate : ''));
        }
      }
    }]);