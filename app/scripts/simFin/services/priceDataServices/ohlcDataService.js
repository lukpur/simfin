/**
 * This is a proxy service which has the actual api consumer injected.
 * This allows us to easily change the API consumer (e.g. to google) if the
 * need arises.
 */
angular.module('simFin.priceDataServices')
    .factory('ohlcDataService', ['yahooDataService', '$q', function(priceDataService, $q) {
      var curSymbol,
          priceData,
          lastQuery,
          lastInterval,
          opts = {
            cache: true,
            cacheTime: 86400000
          };

      return {
        loadPriceData: function(symbol) {
          var curTime = (new Date()).getTime(),
              deferred,
              intervalHasChanged = arguments[1] && arguments[1].interval && arguments[1].interval != lastInterval;
          if (!intervalHasChanged && curSymbol === symbol && opts.cache && lastQuery && (curTime - lastQuery) < opts.cacheTime) {
            deferred = $q.defer();
            deferred.resolve(priceData);
            return deferred.promise;
          } else {
            curSymbol = symbol;
            lastQuery = (new Date()).getTime();
            if (arguments[1]){
              lastInterval = arguments[1].interval;
            }
            return priceDataService.getPriceData.apply(priceDataService, arguments).then(function(response) {
              priceData = response.data;
              return priceData;
            });
          }
        },
        getCurrentSymbol: function() {
          return curSymbol;
        },
        getPriceData: function() {
          return priceData;
        }
      }
    }]);
