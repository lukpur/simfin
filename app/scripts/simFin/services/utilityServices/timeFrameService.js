angular.module('utilityServices')
  .factory('timeFrameService', ['ohlcDataService', function(ohlcDataService) {
    var service = {},
        opts = {
          endDate: new Date(),
          frameSize: 30
        },
        msInADay = 86400000,
        dataFrame;

    var yyyymmddFormatter = function(date) {
      return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    };

    var updateDataFrame = function() {
      var rangeMin = service.getStartDate().getTime(),
          rangeMax = service.getEndDate().getTime();
      dataFrame = {};
      angular.forEach(ohlcDataService.getPriceData(), function(item, i) {
        if(i < rangeMax && i > rangeMin) {
          dataFrame[i] = item;
        }
      });
    };

    service.setEndDate = function(newDate) {
      var newAsDate;
      try {
        newAsDate = new Date(newDate);
        opts.endDate = newAsDate;
        updateDataFrame();
        return true;
      } catch(e) {
        return false;
      }
    };

    service.setFrameSize = function(newFrameSize) {
      if (!isNaN(+newFrameSize) && newFrameSize > 0) {
        opts.frameSize = newFrameSize;
        updateDataFrame();
        return true;
      }
      return false;
    };

    service.getStartDate = function() {
      var days = opts.frameSize * msInADay,
          retDate = new Date(opts.endDate.getTime());

      retDate.setTime(retDate.getTime() - days);
      return retDate;
    };

    service.getStartDateYYYYMMDD = function() {
      return yyyymmddFormatter(service.getStartDate());
    };

    service.getEndDate = function() {
      return opts.endDate;
    };

    service.getEndDateYYYYMMDD = function() {
      return yyyymmddFormatter(service.getEndDate());
    };

    service.getDataFrame = function() {
      updateDataFrame();
      return dataFrame;
    }

    return service;

  }]);