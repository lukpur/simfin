angular.module('d3Utilities')
  .factory('xScaleService', ['d3Service', 'config', function(d3Service, config) {
      var service = {},
          currentScale,
          oldScale,
          d3 = d3Service.getD3(),
          xPadding = config.xScalePaddingPercent || 0.02;

      service.createNewScale = function(keys, plotWidth) {
        var xScalePadding = ~~((keys[keys.length-1] - keys[0]) * xPadding),
            xScale = d3.time.scale().range([0, plotWidth])
                .domain(["" + (+keys[0] - xScalePadding), "" + (+keys[keys.length - 1] + xScalePadding)]);
        currentScale = xScale;

        return currentScale;
      };

      service.getOldScale = function() {
        return oldScale;
      };

      service.getScale = function() {
        return currentScale;
      };

      return service;
    }])