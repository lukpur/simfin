angular.module('simFin.configurationService', [])
    .constant('priceDataEndpoint', '/api/pricedata/')
    .constant('config', {
        minWidth: '400px',
        margin: {
          top: 30,
          right: 30,
          bottom: 50,
          left: 70
        },
        yScalePaddingPercent: 0.07,
        xScalePaddingPercent: 0.02,
        notchWidth: 2,
        transitionDuration: 800
    });