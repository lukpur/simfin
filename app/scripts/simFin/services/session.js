'use strict';

angular.module('simFinApp')
  .factory('Session', function ($resource) {
    return $resource('/api/session/');
  });
